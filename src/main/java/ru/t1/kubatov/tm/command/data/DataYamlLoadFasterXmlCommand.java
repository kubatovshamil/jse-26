package ru.t1.kubatov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kubatov.tm.dto.Domain;
import ru.t1.kubatov.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("DATA LOAD YAML");
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_YAML));
        @NotNull final String yaml = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
        setDomain(domain);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from yaml file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-load-yaml";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
