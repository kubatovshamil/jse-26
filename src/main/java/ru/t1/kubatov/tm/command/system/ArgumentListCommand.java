package ru.t1.kubatov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.kubatov.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentListCommand extends AbstractSystemCommand {

    @NotNull
    public final static String DESCRIPTION = "Display allowed arguments.";

    @NotNull
    public final static String NAME = "arguments";

    @NotNull
    public final static String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            @NotNull final String argument = command.getArgument();
            if (argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }
}
