package ru.t1.kubatov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kubatov.tm.api.repository.IUserRepository;
import ru.t1.kubatov.tm.enumerated.Role;
import ru.t1.kubatov.tm.model.User;

public interface IUserService extends IUserRepository {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    User deleteByLogin(@Nullable String login);

    @NotNull
    User deleteByEmail(@Nullable String email);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User updateUser(@Nullable String id, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
